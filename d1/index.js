console.log("Hello World");

//ES6 Updates
	/*
		ECMAScript is the meaning of ES.

		ES6 is one of the latest versions of writing JS and in fact is one of the major updates to JS.

		let, const - are ES6 updates, these are the new standards of creating variables.
	*/



//Exponent Operator
	/*
		
	*/
	//this is the updated Exponent Operator
	const firstNum = 8 ** 2;
	console.log(firstNum);

	//Old Exponent Operator
	const secondNum = Math.pow(8, 2);
	console.log(secondNum);



	/*
		Mini Activity
		1. create new variable called sentece1 and sentence 2
		2. Concatenate and save a resulting string into sentence1
		3. concatenate and save a resulting string into sentence2
			log both variables in the console and take a screenshot.
			the sentences must have spaces and punctuation.
	*/
	let string1 = "fun";
	let string2 = "Bootcamp";
	let string3 = "Coding";
	let string4 = "Javascript";
	let string5 = "Zuitt";
	let string6 = "Learning";
	let string7 = "Love";
	let string8 = "I";
	let string9 = "is";
	let string10 = "in";

	let sentence1 = `${string8} ${string7} ${string3}.`
	let sentence2 = `${string5} ${string2} ${string9} ${string1}.`
	console.log(sentence1);
	console.log(sentence2);


//Templete Literals
	/*
		- Templete Literals Allow us to create strings using `` and easily embed JS expression in it, it allows us to write strings without using the concatenation operator (+)
		- Greatly helps with code readability.

		" ", " " = String Literal. This is the old way.

		${} is a placeholder that is used to embed JS expressions when creating strings using Template Literals. We are using ``(backticks)
	*/
	let sentence3 = `${string8} ${string7} ${string5} ${string3} ${string2}!`
	console.log(sentence3);



//Multi-line Using Templete Literals
	/*
		We use this \n in the old way to make a new line or parang tab o enter.

		Pero sa ES6 hindi na, para nalang tayong nag tataype ng normal paragraph.
	*/
	let name = "John";
	const anotherMessage = `
		${name} attended  a math competition.
		he won it by solving the problem 8 ** 2 with the solution of ${firstNum}.
	`
	console.log(anotherMessage);

	//using Tamplate Literals in console.log for objects.
	let dev = {
		name: "peter",
		lastName: "Parker",
		occupation: "web developer",
		income: 50000,
		expenses: 60000
	};
	console.log(`${dev.name} is a ${dev.occupation}`);
	console.log(`His income is ${dev.income} and expenses at ${dev.expenses}. His surrent balance is ${dev.income -  dev.expenses}.`);

	const interestRate = .1;
	const principal = 1000;
	console.log(`The interest on your savins account is ${principal * interestRate}.`);



//Array Destructuring
	/*
		- Allows us to unpack elements in arrays into distinct variables.

		Syntax:
			let [variableNameA, variableNameB, variableNameC,] = array;
	*/
	const fullName = ["Juan", "Dela", "Cruz", "Y", "Rizal"];

	//Old way of Array Destructing
	console.log(fullName[0]);
	console.log(fullName[1]);
	console.log(fullName[2]);

	console.log(`Hello ${fullName[0]} ${fullName[1]} ${fullName[2]}! It is nice to meet you.`);

	//This is the new way of array destructuring.
	const [firstName, middleName, lastName, anotherName, yhyhy] = fullName;
	console.log(firstName);
	console.log(middleName);
	console.log(lastName);
	console.log(yhyhy);

	/*
		Instead of using index, we can access the array using it's name.
	*/



//Object Destructuring
	/*
		- Allows us to unpack properties of objects into dictint variables.

		Syntax:
			let {propertyNameA, propertyNameB, propertyNameC,} = object;
	*/
	const person = {
		givenName: "Jane",
		maidenName: "Dela",
		familyName: "Cruz"
	};

	//Old way of Object Destructuring.
	console.log(person.givenName);
	console.log(person.maidenName);
	console.log(person.familyName);

	//New way of Object Destructuring.
	let {givenName, maidenName, familyName} = person;
	console.log(givenName);
	console.log(maidenName);
	console.log(familyName);



//Arrow Functions
	/*
		- Compact alternative syntax to traditional functions.
		- Used in anonymous functions.
		- Arrow Function is just an alternative syntax.
		- It's your own taste kung ano ang gagamitin.
	*/

	const hello = () => {
		console.log("Hello 211!");
	};
	hello();

	/*const hello = function hello1(){
		console.log("Hello Again");
	};
	hello();*/


	//old way of arrow functon and Template Literals
	/*
		Syntax:
			function functionName(parameterA, parameterB, parameterC,){
				console.log();
			};
	*/


	//new way of arrow function
	/*
		Syntax:
			let variableName = (parameterA, parameterB, parameterC,) => {
				console.log();
			}
	*/
	//New arrow function
	const printFullName = (firsName, middleName, lastName) => {
		console.log(`${firsName} ${middleName}, ${lastName}`);
	};
	printFullName("John", "D", "Smith")

	//arrow function with loops
	//old arrow function
	const students = ["John", "Jane", "Judy"];

	students.forEach(function(student){
		console.log(`${student} is a student`);
	});

	//new arrow function
	students.forEach((student)=>{
		console.log(`${student} is a student`);
	});

	//puwede ring wala ng {}
	/*students.forEach((student)=> console.log(`${student} is a student`));*/



//Implicit Return Statement
	/*
		- There are instances when you can omit the "return" statement.
		- This works because even without the "return" statement JS IMPLICITLY adds it for the result of the function.
	*/

	//Pre-arrow function
	/*const add = (x,y) => {
		return x + y;
	}
	let total = add(1,2);
	console.log(total);*/

	//{} in an arrow function are code block. If an arrow function has a {} or code block, we're going to need to use a return.

	//New-arrow function
	const add = (x,y) => x+y;
	let total = add(1,2);
	console.log(total);


	/*
		Mini Activity
		create a subtract, multiply, and divide arrow function use 1 and 2 as your arguments.
		log the total in your console.
	*/
	const subtract = (x,y) => x-y;
	let difference = subtract(1,2);
	console.log(difference);

	const multiply = (x,y) => x*y;
	let product = multiply(1,2);
	console.log(product);

	const divide = (x,y) => x/y;
	let quotient = divide(1,2);
	console.log(quotient);


//Default Function Argument Value
	/*
		- We provide  a default argument value if none is proveided when the function is invoked.
	*/
	const greet = (name = "User") => {
		return `Good Evening, ${name}`;
	}
	console.log(greet());
	console.log(greet("John"));
	//Here the default value is "User" that we put in the parameter.


//Class-Based Object Blueprint
	/*
		- This allows creation or instantiation of objects using classes as blueprints.
	*/
	//creating a class
	/*
		the constructor is a special method of a class for creating or initializing an object fot that class.

		Syntax:
			class className {
				constructor(objectPropertyA, objectPropertyB,){
					this.objectPropertyA = objectPropertyA;
					this.objectPropertyB = objectPropertyB;
				}
			}
	*/
	class Car {
		constructor(brand, name, year){
			this.brand = brand;
			this.name = name;
			this.year = year;
		}
	};
	//Instantiating an object
	/*
		The "new" operator create or instantiates a new object with the given arguments as the value of its properties.
	*/
	// let  variableName = new className();
	/*
		creating a constant with the "const" keyword and assigning it a value of an object makes it so we  can't reassign it with another data type.
	*/
	const myCar = new Car();
	console.log(myCar); //undefined

	myCar.brand = "ford";
	myCar.name = "Everest";
	myCar.year = "1996";
	console.log(myCar);

	const myNewCar = new Car("Toyota", "Vios", 2021);
	console.log(myNewCar);


	/*
		Mini Activity
		create a character class constructor
			name:
			role:
			strenght:
			weakness:

		create 2 new characters out of the class constructor and save it in their respective variables.
	*/
	class Character {
		constructor(name, role, strength, weakness){
			this.name = name;
			this.role = role;
			this.strength = strength;
			this.weakness = weakness;
		}
	};

	const character1 = new Character("Mr. Pogi", "Savior", "Over confident","No Power");
	console.log(character1);
	const character2 = new Character("Goku", "Bida", "Super Saiyan","Wife");
	console.log(character2);