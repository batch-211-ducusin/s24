console.log("Hello World");

/*
1. In the S24 folder, create an activity folder and an index.html and index.js file inside of it.
2. Link the index.js file to the index.html file.
3. Create a variable getCube and use the exponent operator to compute for the cube of a number. (A cube is any number raised to 3)
*/
const getCube = 5 ** 3;

/*
// Template Literals
4. Using Template Literals, print out the value of the getCube variable with a message of The cube of <num> is…
5. Create a variable address with a value of an array containing details of an address.
6. Destructure the array and print out a message with the full address using Template Literals.
*/

console.log(`The cude of 5 is ${getCube}`);
const address = ["Cabasaan St", "Pinagsama", "Taguig City"];

const [street, barangay, city] = address;
console.log(`I live at ${street} ${barangay}, ${city}`);

/*
// Object Destructuring
7. Create a variable animal with a value of an object data type with different animal details as it’s properties.
8. Destructure the object and print out a message with the details of the animal using Template Literals.
*/

const animalInfo = {
	name: "Lolong",
	type: "Saltwater Crocodile",
	weight: "1075 kgs",
	lenght: "20 ft 3 in"
};

const {name, type, weight, lenght} = animalInfo;
console.log(`${name} was a ${type}. He weighed at ${weight} with a measurement of ${lenght}.`);

/*
// Arrow Functions
9. Create an array of numbers.
*/

const number = [1, 2, 3, 4, 5];

/*
// A
10. Loop through the array using forEach, an arrow function and using the implicit return statement to print out the numbers.
*/

number.forEach((num) => console.log(num));

/*
// B
11. Create a variable reduceNumber and using the reduce array method and an arrow function console log the sum of all the numbers in the array.
*/

const reduceNumber = number
const reducer = (accumulator, curr) => accumulator + curr;

console.log(reduceNumber.reduce(reducer));

/*
// Javascript Objects
12. Create a class of a Dog and a constructor that will accept a name, age and breed as it’s properties.
13. Create/instantiate a new object from the class Dog and console log the object.
*/

class Dog {
	constructor(name, age, breed){
		this.name = name;
		this.age = age;
		this.breed = breed;
	}
}

const myDog = new Dog("Frankie", 5, "Miniature Dachshund");
console.log(myDog);

/*
14. Create a git repository named S24.
15. Initialize a local git repository, add the remote link and push to git with the commit message of "Add s24 activity code".
16. Add the link in Boodle.
*/